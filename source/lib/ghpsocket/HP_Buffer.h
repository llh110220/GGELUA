﻿#pragma once
#include "common.h"
struct HP_Header
{
	//0包头，1序号，2长度，3数量
	int flag;	//标识
	int seq;	//序号
	int len;	//长度
	int qty;	//数量
};

class HP_Buffer
{
public:
	HP_Buffer(int flag, int len, int max_seq);
	~HP_Buffer(void);
	void Reset();
	bool AddNumber(int v);
	bool AddString(const char* v, int len);
	void Finish();
	unsigned int GetHeaderLen();
	unsigned int CheckHeader();
	unsigned char* GetPtr();
	unsigned int GetLen();
	void SetCode(int* psd, int len);
	void SetComp(bool v);

	int m_flag;
	int m_seq;
	int m_len;
	int m_qty;
	int m_maxseq;
	unsigned int m_bodylen;
	unsigned int m_buflen;
	unsigned char* m_data;
	HP_Header* m_head;
	int* m_wint;
	int* m_rint;
	bool m_lock;
	int m_i;
	int* m_code;
	int m_codelen;
	bool m_comp;
};
