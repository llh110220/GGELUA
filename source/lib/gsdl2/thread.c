#include "gge.h"
#include "SDL_thread.h"
//
//typedef struct _THREADINFO
//{
//	const char* ggecore;
//	size_t coresize;
//	const char* entry;
//	int isdebug;
//	int argc;
//	char* argv[];
//	SDL_Thread* th;
//}GGE_Thread;
//
//static int ThreadFunction(void *data)
//{
//	GGE_Thread* info = (GGE_Thread*)data;
//	lua_State* L = luaL_newstate();
//	luaL_openlibs(L);
//
//	luaL_getsubtable(L, LUA_REGISTRYINDEX, "_ggelua");
//	//lua_createtable(L, argc, 0);
//	//for (int i = 1; i < argc; i++) {
//	//    lua_pushstring(L, argv[i]);
//	//    lua_seti(L, -2, i);
//	//}
//	//lua_setfield(L, -2, "arg");//gge.arg
//#ifdef __WIN32__
//	lua_pushboolean(L, GetConsoleWindow() != NULL);
//#else
//	lua_pushboolean(L, 0);
//#endif
//	lua_setfield(L, -2, "isconsole");//gge.isconsole
//	lua_pushboolean(L, info->isdebug);
//	lua_setfield(L, -2, "isdebug");//gge.isdebug
//	lua_pushstring(L, info->entry);
//	lua_setfield(L, -2, "entry");//gge.entry
//	lua_pushboolean(L, 0);
//	lua_setfield(L, -2, "ismain");//gge.ismain
//	lua_pushstring(L, SDL_GetPlatform());
//	lua_setfield(L, -2, "platform");//gge.platform
//	lua_setglobal(L, "gge");
//
//	lua_getfield(L, LUA_REGISTRYINDEX, LUA_PRELOAD_TABLE);//package.preload
//	lua_pushcfunction(L, luaopen_ggelua);
//	lua_setfield(L, -2, "ggelua");
//	lua_pop(L, 1);
//
//	SDL_mutex** extra = (SDL_mutex**)lua_getextraspace(L);
//	SDL_mutex* mutex = SDL_CreateMutex();
//	*extra = mutex;
//
//	size_t coresize = info->coresize;
//	const char* ggecore = info->ggecore;
//
//	SDL_LockMutex(mutex);
//	if (luaL_loadbuffer(L, ggecore, coresize, "ggelua.lua") == LUA_OK) {
//		lua_pushstring(L, info->entry);
//		lua_pushboolean(L, 1);//�Ƿ������
//
//		if (lua_pcall(L, 2, 0, 0) == LUA_OK) {
//			if (lua_getglobal(L, "main") == LUA_TFUNCTION) {//loop
//				if (lua_pcall(L, 0, 0, 0) != LUA_OK)
//					printf("%s\n", lua_tostring(L, -1));
//			}
//		}
//	}
//
//	SDL_DestroyMutex(mutex);
//	lua_close(L);
//	SDL_free(info);
//	return 0;
//}
//
//static int  LUA_CreateThread(lua_State *L)
//{
//	if (lua_getfield(L, LUA_REGISTRYINDEX, "ggecore") == LUA_TSTRING)
//	{
//		GGE_Thread* info = (GGE_Thread*)SDL_malloc(sizeof(GGE_Thread));
//		info->entry = luaL_checkstring(L, 1);
//		info->ggecore = lua_tolstring(L, -1, &info->coresize);
//		info->th = SDL_CreateThread(ThreadFunction, NULL, (void*)info);
//		lua_pushboolean(L, info->th != NULL);
//		if (!info->th)
//			SDL_free(info);
//		return 1;
//	}
//    return 0;
//}
////SDL_CreateThreadWithStackSize
//static int  LUA_GetThreadName(lua_State *L)
//{
//    GGE_Thread * ud = (GGE_Thread*)luaL_checkudata(L, 1, "SDL_Thread");
//    lua_pushstring(L,SDL_GetThreadName(ud->th));
//    return 1;
//}
//
//static int  LUA_GetThreadID(lua_State *L)
//{
//    GGE_Thread * ud = (GGE_Thread*)luaL_checkudata(L, 1, "SDL_Thread");
//    lua_pushinteger(L,SDL_GetThreadID(ud->th));
//    return 1;
//}
//
//static int  LUA_SetThreadPriority(lua_State *L)
//{
//    static const char *const opts[] = {"SDL_THREAD_PRIORITY_LOW", "SDL_THREAD_PRIORITY_NORMAL", "SDL_THREAD_PRIORITY_HIGH","SDL_THREAD_PRIORITY_TIME_CRITICAL", NULL};
//    
//    lua_pushinteger(L,SDL_SetThreadPriority((SDL_ThreadPriority)luaL_checkoption(L, 2, NULL, opts)));
//    return 1;
//}
//
//static int  LUA_WaitThread(lua_State *L)
//{
//    GGE_Thread * ud = (GGE_Thread*)luaL_checkudata(L, 1, "SDL_Thread");

//    return 0;
//}
//
//static int  LUA_DetachThread(lua_State *L)
//{
//    GGE_Thread * ud = (GGE_Thread*)luaL_checkudata(L, 1, "SDL_Thread");
//    SDL_DetachThread(ud->th);
//    ud->th = NULL;
//    return 1;
//}
////SDL_TLSCreate
////SDL_TLSGet
////SDL_TLSSet
static int  LUA_ThreadID(lua_State* L)
{
	lua_pushinteger(L, SDL_ThreadID());
	return 1;
}
//static const luaL_Reg thread_funcs[] = {
//    {"GetThreadName"     , LUA_GetThreadName}     ,
//    {"GetThreadID"    , LUA_GetThreadID}    ,
//    {"SetThreadPriority"     , LUA_SetThreadPriority}     ,
//    {"WaitThread"    , LUA_WaitThread}    ,
//    {"DetachThread"    , LUA_DetachThread}    ,
//    { NULL, NULL}
//};
//
static const luaL_Reg sdl_funcs[] = {
    //{"CreateThread"          , LUA_CreateThread}   ,  
    {"ThreadID"          , LUA_ThreadID}   ,
    { NULL, NULL}
};

int bind_thread(lua_State *L)
{
    //luaL_newmetatable(L,"SDL_Thread");
    //luaL_setfuncs(L,thread_funcs,0);
    //lua_pushvalue(L, -1);
    //lua_setfield(L, -2, "__index");
    //lua_pop(L, 1);

    luaL_setfuncs(L,sdl_funcs,0);
    return 0;
}