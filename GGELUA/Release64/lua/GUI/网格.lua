--[[
    @Author       : baidwwy
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-03-09 04:06:52
--]]

local _ENV = require("GGE界面")

GUI网格 = class("GUI网格",GUI控件)
GUI网格._type    = 9
function GUI网格:初始化()
    
    self.行间距   = 0
    self.列间距   = 0
    self.格子宽度 = 50
    self.格子高度 = 50
    self.行数量   = 0
    self.列数量   = 0
end

function GUI网格:置格子宽高(a,b)
    self.格子宽度 = a
    self.格子高度 = b
    return self
end

function GUI网格:置行列间距(a,b)
    self.行间距 = a
    self.列间距 = b
    return self
end

function GUI网格:置行列数量(a,b)
    self:释放()
    self.行数量 = a
    self.列数量 = b
    for h=1,self.行数量 do
        for l=1,self.列数量 do
            self:添加格子(
                (l-1)*(self.格子宽度+self.列间距),
                (h-1)*(self.格子高度+self.行间距)
            )
        end
    end
    return self
end

function GUI网格:添加格子(x,y,w,h)
    local id = #self.子控件+1
    local r = self:创建控件(id,x,y,w or self.格子宽度,h or self.格子高度)
    if type(self.子显示)=='function' then
        r.显示 = function (this,...)
        local x,y = ...
            self:子显示(x,y,this.名称)
        end
    end
    return self
end

function GUI网格:检查格子(x,y)
    if self:检查点(x,y) then
        for i,v in ipairs(self.子控件) do
            if v.是否可见 and v:检查点(x,y) then
                return i,v
            end
        end
    end
end

function GUI网格:_消息事件(msg)
    if self.是否禁止 then
        return 
    end
    GUI控件._消息事件(self,msg)

    for _,v in ipairs(msg.鼠标) do
        if v.type==SDL.鼠标_按下 then
            local a,b = self:检查格子(v.x,v.y)
            if a then
                v.type = nil
                local x,y = b:取坐标()
                if v.button==SDL.BUTTON_LEFT then
                    self._ldown = a
                    self:发送消息("左键按下",x,y,a,b,msg)
                elseif v.button==SDL.BUTTON_RIGHT then
                    self._rdown = a
                    self:发送消息("右键按下",x,y,a,b,msg)
                end
            end
        elseif v.type==SDL.鼠标_弹起 then
            local a,b = self:检查格子(v.x,v.y)
            if a then
                v.type = nil
                local x,y = b:取坐标()
                if v.button==SDL.BUTTON_LEFT then
                    if self._ldown == a then
                        self:发送消息("左键弹起",x,y,a,b,msg)
                    end
                elseif v.button==SDL.BUTTON_RIGHT then
                    if self._rdown == a then
                        self:发送消息("右键弹起",x,y,a,b,msg)
                    end
                end
            end
            self._ldown = nil
            self._rdown = nil
        elseif v.type==SDL.鼠标_移动 then
            if v.state==0 then
                local a,b = self:检查格子(v.x,v.y)
                if a then
                    v.type = nil
                    self._focus = true
                    local x,y = b:取坐标()
                    self:发送消息("获得鼠标",x,y,a,b,msg)
                elseif self._focus then
                    self._focus = nil
                    self:发送消息("失去鼠标",v.x,v.y,msg)
                end
            end
        end
    end
    
end

