--[[
    @Author       : GGELUA
    @Date         : 2021-04-24 17:17:10
    @LastEditTime : 2021-05-08 02:03:11
--]]

local im = require"gimgui"
local IMBase = require"IMGUI.基类"

local IM菜单 = class('IM菜单',"IMBase")

function IM菜单:初始化(name,enabled)
    self._name = name
    self._enabled = enabled
end

function IM菜单:开始()
    return im.BeginMenu(self._name,self._enabled)
end

function IM菜单:结束()
    return im.EndMenu()
end

function IM菜单:置禁止(enabled)
    self._enabled = enabled
    return self
end

--=====================================================
local IM顶部菜单 = class('IM顶部菜单',"IMBase")
package.loaded["IMGUI.顶部菜单"] = IM顶部菜单

function IM顶部菜单:开始()
    return im.BeginMainMenuBar()
end

function IM顶部菜单:结束()
    return im.EndMainMenuBar()
end

--=====================================================
local IM窗口菜单 = class('IM窗口菜单',"IMBase")
package.loaded["IMGUI.窗口菜单"] = IM窗口菜单

function IM窗口菜单:开始()
    return im.BeginMenuBar()
end

function IM窗口菜单:结束()
    return im.EndMenuBar()
end

--=====================================================
local IM菜单选项 = class('IM菜单选项',"IMBase")
package.loaded["IMGUI.菜单选项"] = IM菜单选项

function IM菜单选项:初始化(name,shortcut)
    self._name = name
    self._shortcut = shortcut
end

function IM菜单选项:显示()
    if self._selected~=nil then
        self._selected = im.MenuItem(self._name,self._shortcut,self._selected,self._enabled)
    else
        return im.MenuItem(self._name,self._shortcut,false,self._enabled)
    end
    return self._selected
end

function IM菜单选项:置禁止(enabled)
    self._enabled = enabled
    return self
end

function IM菜单选项:置选中(selected)
    self._selected = selected
    return self
end

function IM菜单选项:是否选中()
    return self._selected
end

return IM菜单